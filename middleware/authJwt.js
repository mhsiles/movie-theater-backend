const jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");

verifyToken = (req, res, next) => {
  // Token de acceso
  let token = req.headers["x-access-token"];

  if (!token) {
    return res.status(403).send({
      message: "Token no existente."
    });
  }

  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      return res.status(401).send({
        message: "Acceso no autorizado."
      });
    }
    req.userId = decoded.id;
    next();
  });
};

isAdmin = (req, res, next) => {
  let role = req.headers["role"];
  if (role === "admin") {
    next();
    return;
  }
  res.status(403).send({
    message: "Acceso restringido."
  });
};


const authJwt = {
  verifyToken: verifyToken,
  isAdmin: isAdmin,
};

module.exports = authJwt;