const db = require("../models");
const Movie = db.movie;

const Sequelize = require("sequelize");

// Find a Movie
exports.findOne = (req, res) => {
  const id = req.params.id;

  Movie.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error obteniendo la película con id: " + id
      });
    });
};

// Get all Movies
exports.findAll = (req, res) => {
  Movie.findAll({
    // ASC order by id
    order: Sequelize.col('id'),
  })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Ocurrió un error."
      });
    });
};

// Create a Movie
exports.create = (req, res) => {
  // Validate request
  if (!req.body.name || !req.body.director || !req.body.year) {
    res.status(400).send({
      message: "Error. Información faltante."
    });
    return;
  }

  const movie = {
    name: req.body.name,
    year: req.body.year,
    director: req.body.director
  };

  // Save Movie in the database
  Movie.create(movie)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Error al crear la película."
      });
    });
};

// Update a Movie
exports.update = (req, res) => {
  const movieId = req.params.id;

  Movie.update(req.body, {
    where: { id: movieId }
  })
  .then(result => {
    if (result == 1) {
      res.send({
        message: "Película actualizada exitosamente."
      });
    } else {
      res.send({
        message: "Error con la información enviada. Favor de revisarla."
      });
    }
  })
  .catch(err => {
    res.status(500).send({
      message: "Error al actualizar la información de la película."
    });
  });
};

// Delete a Movie
exports.delete = (req, res) => {
  const movieId = req.params.id;

  Movie.destroy({
    where: { id: movieId }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Pelicula eliminada exitosamente."
        });
      } else {
        res.send({
          message: "Ocurrió un error inesperado. Favor de revisar la información."
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error al eliminar la película."
      });
    });
};
