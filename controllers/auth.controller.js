const db = require("../models");
const config = require("../config/auth.config");
const User = db.user;

const Op = db.Sequelize.Op;

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

exports.signin = (req, res) => {
  User.findOne({
    where: {
      username: req.body.username
    }
  })
    .then(user => {
      if (!user) {
        return res.status(404).send({ message: "Usuario incorrecto." });
      }

      // Compare encripted password
      var passwordIsValid = bcrypt.compareSync(
        req.body.password,
        user.password
      );

      if (!passwordIsValid) {
        return res.status(401).send({
          message: "Password incorrecto."
        });
      }

      // Create token
      var token = jwt.sign({ id: user.id }, config.secret, {
        expiresIn: 3600 // 1 hour
      });
      
      user.getRoles().then(role => {
        res.status(200).send({
          username: user.username,
          role: role[0].name,
          accessToken: token
        });
      });

    })
    .catch(err => {
      res.status(500).send({ message: err.message });
    });
};