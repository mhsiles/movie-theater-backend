# Backend Demo Proyecto Películas

Movie theater backend project.

## Tecnologías

* Node v14.16.0
* Express v4.16.1
* Postgres

## Requisitos

Dentro de la carpeta "config":

auth.config.js
```
module.exports = {
  secret: "your_secreto"
};
```

db.config.js
```
module.exports = {
  HOST: "localhost",
  USER: "postgres",
  PASSWORD: "password",
  DB: "your_database",
  dialect: "postgres",
  port: "5432",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};
```

## Uso

```
cd carpeta_repositorio
PORT=port node app.js
```
