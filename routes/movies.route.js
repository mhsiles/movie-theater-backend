var express = require('express');
var router = express.Router();
const { authJwt } = require("../middleware");

const movies = require("../controllers/movie.controller.js");

// Retrieve all Movies
router.get("/",
            [authJwt.verifyToken, authJwt.isAdmin],
            movies.findAll);

// Retrieve a Movie
router.get("/:id",
            [authJwt.verifyToken, authJwt.isAdmin],
            movies.findOne);

// Create a Movie
router.post("/",
            [authJwt.verifyToken, authJwt.isAdmin],
            movies.create);

// Update a Movie
router.put("/:id",
            [authJwt.verifyToken, authJwt.isAdmin],
            movies.update);

// Delete a Movie
router.delete("/:id",
              [authJwt.verifyToken, authJwt.isAdmin],
              movies.delete);


module.exports = router;