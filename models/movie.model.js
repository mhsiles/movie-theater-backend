module.exports = (sequelize, Sequelize) => {
  const Movie = sequelize.define("movie", {
    name: {
      type: Sequelize.STRING
    },
    year: {
      type: Sequelize.INTEGER
    },
    director: {
      type: Sequelize.STRING
    }
  });

  return Movie;
};